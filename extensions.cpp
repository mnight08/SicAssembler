#include <iostream>
#include "prototypes.h"
#include "functiontree.h"
#include <string>
#include "alphabet.h"
#include "includelist.h"
using namespace std;

//the main purpose of this class was to allow for functions to be created while 
//inside the simulator.  this file just reads in input, and runs the given command
int main()
{
	//a tree of functions that is going to store all the functions in "includelist.h"
	functiontree commandlist;
	string input1 ="prototypes.h", input2="flags.txt", input3="includelist.h";
	
	//this rewrites the function insertnodes if the file flags.txt conaints zero, otherwise
	//it just runs insert nodes
	commandlist.build(input1,input2,input3);
	string line;
	string temp;
	cout<<"hello!!! how can i help you?"<<endl;
	//list of characters that separate possible tokens
	vector<char> del;del.push_back(' ');del.push_back('\t');
	
	//every function will take one string, the arguments from the terminal 
	while(true)
	{

	cout<<">>>>>>\t";
		getline(cin,line);
		if(line!="")
		{
			//functions->search returns a point to a function pointer, this recieves that
			string (**func)(string&)=NULL;
			//gets the name of the function.
			temp=commandlist.getfirsttoken(line,del);
			//look up the given command to see if it is a valid command
			func=commandlist.functions->search(temp);
			if(func!=NULL)
			{
				//get the arguments passed the function if it is not null
				temp=commandlist.removefirsttoken(line,del);
				//run the given function with those arguments
				(*func)(temp);
			}
			else{
				cout<<"Command not found\a!!  Type help for a list of available commands"<<endl;
			}
		}
		//handle blank line
		else {
		cout<<"You actually have to type things for this to do anything useful!!!!!\a\a\a\a\a\a"<<endl;
		}
	}
	return 0;
}
