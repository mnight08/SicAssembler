#include <string>
#include "functiontree.h"
#include <vector>
#include <fstream>
#include <iomanip>
//function prototypes.  This is for readability, and to allow me to write the 
//function definitions in whatever order.  They are defined in the order of the
//prototypes.  The function definitions contain some comments on them, and what 
//they do.
bool 	iscomment(string &line);
bool 	isvalidlabelchar(char&a,string&letters);
bool	isvalidlabel(string&label,tree<int>&symtab);
bool	haslabel(string&line,vector<char>&del);
bool	isdec(char&d);
bool	isdecimal(string&number);
bool	ishex(string&number);
bool	hasoperand(string&instruction,tree<vector<int> >&mnuemonics,tree<int>&directives);
bool	isindexed(string&operand);
bool	isvalidoperand(string&operand,tree<int>&symtab);
bool 	istextrecordinitialized(string&record);
bool	hasroomformnuemonic(string&record);
bool 	hasroomforbyte(string&record,string&operand);
int 	getaddress(string&operand,tree<int>&symtab,int&lc);
string 	toupper(string s);
string 	tohex(int number);
string 	getmnuemonicobjectcode(string&instruction, string&operand,tree<int>&symtab,tree<vector<int> >&mnuemonics,int&lc);
string 	getwordobjectcode(string&operand);
string 	getbyteobjectcode(string&operand);
string 	getheaderrecord(string&name,string&size,int&address);
string 	getendrecord(string&label,tree<int>&symtab);
string 	geterrormessage(int i);
string 	assemble(string&arg);
void 	setlc(int&lc,string&instruction,string&operand,tree<vector<int> >&mnuemonics,tree<int>&directives);
void 	getmnuemonicerrors(string&label,string&instruction,string&operand,vector<char>&del,tree<vector<int> >&mnuemonics,vector<int>&errors,tree<int>&symtab);
void 	getdirectiveerrors(string&label,string&instruction,string&operand,vector<char>&del,tree<int>&directives,vector<int>&errors,tree<bool>&instructionsused,tree<int>&symtab);
void 	getlabelerrors(string&label,vector<char>&del,tree<int >&symtab,tree<vector<int> >&mnuemonics,tree<int>&directives,vector<int>&errors);
void 	getoperanderrors(string&errors,string&instruction,string&operand,tree<int>&symtab,tree<vector<int > >&mnuemonics);
void 	getfinalpass1errors(vector<string>&instructions,vector<int>&errors,tree<bool>&instructionsused,int&start,int&end);
void 	getfinalpass2errors(string&errors);
void 	builddirectives(tree<int>&directives);
void 	buildmnuemonics(tree<vector<int> >&mnuemonics);
void 	initializetextrecord(string&record,string&lcs);
void 	writerecords(vector<string>&records, ofstream&output);
void 	finalizetextrecord(string&record,int&start,int&end);
void 	writeintmfileln(string&currentline,int&linenumber,int&lc,string&instruction,string&operand,string&comment,vector<int>&errors,ofstream&intmfile);
void 	writelistingfileln(ofstream&listingfile,string&currentline,string&objectcode,string &loadaddress,string&errors);
void 	writesymtabtolistingfile(ofstream&listingfile,tree<int>&symtab);
void 	writesymtabtolistingfile(ofstream &listingfile, Node<int> *current, string key,tree<int> &symtab);
void 	pass1(ifstream&in,tree<vector<int> >&mnuemonics,tree<int>&directives,tree<int>&symtab,ofstream&intmfile);
void 	pass2(ofstream&output,ofstream&listingfile,ifstream&intmfile,tree<int>&symtab,tree<vector<int> >&mnuemonics);

//checks if the line is a comment
bool iscomment(string &line)
{
	if(line.size()!=0&&line[0]=='.')
	{
		return true;
	}
	else return false;
}

//checks if the character is in the string letters, where the string letters
//is the set of valid characters
bool isvalidlabelchar(char &a, string &letters)
{
	for(int i=0;i<letters.size();i++)
	{	
		if(letters[i]==a)
		{
			return true;
		}
	}
}

//checks if label violates any rules for labels.
bool isvalidlabel(string &label, tree<int> &symtab)
{
	if(label.size() > 7||label.size()<=0)
	{
		return false;
	}
	//first letter is not a character
	if(!(int(label[0])>=int('a')&&int(label[0])<=int('z')||int(label[0])>=int('A')&&int(label[0])<=int('Z')))
	{
		return false; 
	}
	for(int i=1; i< label.size();i++)
	{
		if(!isvalidlabelchar(label[i],symtab.table.letters))
		{
			return false;
		}
	}
	return true;	
}

//check if the line has a label or not
bool haslabel(string &line, vector<char> &del)
{
	if(line.size()==0)
	{
		return false;
	}
	if(iscomment(line)) 
	{
		return false;
	}
	for(int i=0;i<del.size();i++)
	{
		if(line[0]==del[i])
		{
			return false;
		}
	}	
	return true;
}

//check if a character is decimal
bool isdec(char &d)
{
	if(int(d)<int('0')||int(d)>int('9'))
	{
		return false;
	}
	else 
	{
		return true;	
	}
}

//check if the string is just decimal characters
bool isdecimal(string &number)
{
	if(number.size()<1)
	{
		return false;
	}
	else
	{
		for(int i=0;i<number.size();i++)
		{
			if(number[i]<int('0')||number[i]>int('9'))
			{
				return false;
			}
		}
	}
	return true;
}

//check if the string number is a valid hex string
bool ishex(string &number)
{
	if(number.size()<1)
	{
		return false;
	}
	//hex value must start with a decimal number. so if it were f3234, it would need to be
	//0f3234
	else if((int('a')<=tolower(number[0])&&tolower(number[0])<=int('f'))||!isdec(number[0]))
	{
		return false;
	}
	else
	{
		for(int i=1;i<number.size();i++)
		{
			if(!((isdec(number[i]))||(int('a')<=tolower(number[i])&&tolower(number[i])<=int('f'))))
			{
				return false;
			}
		}
	}
	return true;
}

//this is an overly complicated way of checking if an instruction is not RSUB.  
//this would allow for more instructions that dont take arguments 
bool hasoperand(string &instruction, tree<vector<int> > &mnuemonics, tree<int> &directives)
{
	if(mnuemonics.search(instruction)!=NULL)
	{
		if((*(mnuemonics.search(instruction)))[0]>0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else if(directives.search(instruction)!=NULL)
	{
		return true;	
	}
	else
	{
		return false;
	}
}

//check if the operand is indexed
bool isindexed(string &operand)
{
	//set up delimeters between actual label, and the ,X if it is there
	vector<char> del;del.push_back(',');
	//remove the label, and comma from operand
	string temp=functiontree::removefirsttoken(operand,del);
	//if there is an "X" left over, then it is indexed
	if(temp=="X")
	{
		return true;
	}
	//otherwise it is not
	else
	{
		return false;
	}
}


//checks if operand is a valid operand.  this does not check if it is defined.
bool isvalidoperand(string &operand, tree<int> &symtab)
{
	vector<char> del; del.push_back(',');
	string op="";
	string X="";
	op=functiontree::getfirsttoken(operand,del);
	X=functiontree::removefirsttoken(operand,del);
	//either operand is a hex value, a label, or it is indexed in which case
	//everything before the comma should be a hex value, and everything 
	//after the comma should be 'X'
	if(ishex(operand)||isvalidlabel(operand,symtab)||(ishex(op)&&toupper(X)=="X")||isvalidlabel(op,symtab))	
	{
		return true;
	}
	else 
	{
		return false;
	}
}

bool istextrecordinitialized(string &record)
{
	if(record==""||record[0]!='T')
	{
		return false;
	}
	else
	{
		return true;
	}
}

//check if the record has room for the object code generated by a mnuemonic instruction
bool hasroomformnuemonic(string &record)
{
	//check if there is room in the text record for a three byte instruction
	if(record.size()+6<=69)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//check if the record has room for a byte instruction
bool hasroomforbyte(string &record, string &operand)
{
	//if the operand is a string, it starts with a C
	if(toupper(operand[0])=='C')
	{
		//if it is a string, then each character generates
		//two hex characters.  the number of characters is 
		//operand.size()-3, because "'","'",and "C"
		if(2*(operand.size()-3)+record.size()<=69)
		{
			return true;
		}
		else 
		{	//get a string form of temp

			return false;
		}
	}
	//if it is hex string, then each character is one hex character
	else if(toupper(operand[0]=='X'))
	{
		if(operand.size()-3+record.size()<=69)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	return true;
}

//get the address that the operand has, whether it is indexed or not
//assumes the label is defined.  a different function must flag an error with
//the operand before you call this function otherwise it may cause a segmentation 
//fault.  there are other functions to hadel errors
int getaddress(string &operand, tree<int> &symtab, int &lc)
{
	int temp=0;
	if(ishex(operand))
	{
		sscanf(operand.c_str(),"%x",&temp);
	}
	else if(!isindexed(operand))
	{	
		temp= *(symtab.search(operand));
	}
	else
	{	
		vector<char> del; del.push_back(',');string op=functiontree::getfirsttoken(operand,del);
		if(ishex(op))
		{
			sscanf(operand.c_str(),"%x",&temp);
		}
		else
		{
			temp=*(symtab.search(op));
		}
	}
	return temp;
}

//make a string uppercased.
string toupper(string s)
{
	for(int i=0;i<s.size();i++)
	{
		s[i]=toupper(s[i]);
	}
	return s;
}

//convert an integer into a hex string
string tohex(int number)
{
	//c-str large enough for any useable hex string
	char hex[17];
	//used to get a string form of hex
	string temp;
	//output the number into the c-str hex with hex formatting
	sprintf(hex,"%x",number);
	//get a string form of hex
	temp=hex;
	//return hex string of number	
	return temp;
}

//this gets the object code cooresponding to a mnuemonic instruction
string getmnuemonicobjectcode(string &instruction, string &operand,tree<int> &symtab, tree<vector<int> > &mnuemonics, int &lc)
{
	//if the operand is indexed, op would be everything before the ,X
	vector<char> del;del.push_back(',');
	string op="";
	//what the function will return. puts leading zeroes
	string out="000000";
	string temp;
	int index=0;
	int address=0;
	int opcode=0;
	//integer value representing the object code
	int objectcode=0;
	//remove the ,X if it is there, otherwise op=""
	op=functiontree::getfirsttoken(operand,del);
	//opcode is the the mnuemonic code, followed by 16 zeroes
	opcode=(*mnuemonics.search(instruction))[1]<<16;
	if(isindexed(operand))
	{
		//if the operand is indexed, set the 16th bit to 1, assuming it is zero
		index=0x8000;
	}
	//if the mnuemonic takes an arugment, get the address of the operand
	if((*mnuemonics.search(instruction))[0]!=0)
	{
		address=getaddress(operand,symtab,lc);
	}
	//the object code is the sum of the index bit, opcode, and address if they are all 
	//shifted correctly
	objectcode=opcode+index+address;
	temp=tohex(objectcode);
	//keep the leading zeroes that are in out
	for(int i=0;i<temp.size();i++)
	{
		out[5-i]=temp[temp.size()-i-1];

	}
	//return hex form of object code, with leading zeroes
	return out;
}

//returns the object code for a word constant
string getwordobjectcode(string &operand)
{
	string out="000000";
	string s;
	//convert the operand to an integer
	int value=atoi(operand.c_str());
	s=tohex(value);
	//keep the leading zeroes in out string
	for(int i=0;i<operand.size();i++)
	{
		out[5-i]=s[s.size()-1-i];
	}
	return out;
}

//returns the object code for a byte constant
string getbyteobjectcode(string &operand)
{
	string temp;
	string out="";
	//skip the first two characters, and put the rest in temp
	temp=operand.substr(2,operand.size()-3);
	if(toupper(operand[0])=='C')
	{
		for(int i=0;i<temp.size();i++)
		{
			out+=tohex(int(temp[i]));
		}
	}
	else if(toupper(operand[0]=='X'))
	{
		out=temp;
	}
	return out;
}


//return the header record for a program
string getheaderrecord(string &name, string &size, int &address)
{
	string out="H";
	string start=tohex(address);
	out+="      000000000000";
	//put the name in the first six letters
	if(name.size()>6)
	{
		for(int i=0;i<6;i++)
		{
			out[i+1]=name[i];
		}
	}
	else 
	{
		for(int i=0;i<name.size();i++)
		{
			out[i+1]=name[i];
		}
	}
	//put thee size in the last twelve characters
	for(int i=0;i<size.size();i++)
	{
		out[18-i]=size[size.size()-1-i];	
	}
	for(int i=0;i<start.size();i++)
	{	
		out[12-i]=start[start.size()-1-i]; 
	}
	return out;
}

//return the end record for a program
string getendrecord(string &label, tree<int> &symtab)
{
	string out="E000000";
	string temp="";
	int address=*(symtab.search(label));
	temp=tohex(address);
	//put address in out, and leave leading zeroes
	for(int i=0;i<temp.size();i++)
	{	

		out[6-i]=temp[temp.size()-1-i];
	}	
	return out;
}

//return the error message for an error code
string geterrormessage(int i)
{
	switch(i)
	{
		case 0:
			return "illegal label";
			break;
		case 1:
			return "too many labels";
			break;
		case 2:
			return "duplicate label";
			break;
		case 3:
			return "invalid instrucion";
			break;
		case 4:
			return "illegal directive operand";
			break;
		case 5:
			return "duplicate START statement";
			break;
		case 6:
			return "misplaced START statement";
			break;
		case 7:
			return "illegal start operand";
			break;
		case 8:
			return "operand is too large ";
			break;
		case 9:
			return "operand is negative, and thus invalid";
			break;
		case 10:
			return "duplicate END statement";
			break;
		case 11:
			return "illegal or missing operand for end";
			break;
		case 12:
			return "illegal Byte operand";
			break;
		case 13:
			return "operand should be in decimal";
			break;
		case 14:
			return "missing operand";
			break;
		case 15:
			return "odd size hex string";
			break;
		case 16:
			return "character string too long for Byte directive";
			break;
		case 17:
			return "program too long, or underflow";
			break;
		case 18:
			return "missing end statement";
			break;
		case 19:
			return "misplaced end statement";
			break;
		case 20:
			return "misplaced start statement";
			break;
		case 21:
			return "missing name of program";
			break;
		case 22:
			return "missing directive operand";
			break;
		case 23:
			return "missing operand";
			break;
		case 24:
			return "undefined operand";
			break;
		case 25:
			return "hex string too long";
			break;
		case 26:
			return "illegal mnuemonic operand";
			break;
		case 27:
			return "invalid operand";
			break;
		default:
			return "MISSING ERROR MESSAGE!! OHH NOOHHH";
			break;
	}
}

//this is the command called by the command interpreter.  this takes the argument 
//file name, opens it, and runs pass one, and two on it.  at least thats what it will do when
//phase 2, and 3 are done. this function produces a listing file to be used by load.
string assemble(string &arg)
{
	//set up what separates tokens in each line.  white space in this case
	vector<char> del; del.push_back(' ');del.push_back('\t');
	//if the argument at the command line was nothing, then this function shouldnt run
	if(arg=="")
	{	
		std::cout<<"too few arguments, the function takes one argument"<<endl;	
		return "";
	}
	//if there is more than one argument, then the function should stop
	else if(functiontree::removefirsttoken(arg,del)!="")
	{	
		std::cout<<"too many arguments, the function only takes one argument"<<endl; 
		return "";
	}
	//the actual assemble part of the function
	else{
		string filename=functiontree::getfirsttoken(arg,del);
		if(filename.substr(filename.size()-4,4)!=".asm")
		{
			std::cout<<"file name must end in .asm!!!"<<endl;
			return "";
		}	
		else 
		{

			//remove .asm extension to name other files
			filename=filename.substr(0,filename.size()-4);
			//intermediate file is named filename.txt
			string intmfilename=filename;
			intmfilename+=".txt";
			//object file name will be filename.obj
			string objectfilename=filename;
			objectfilename+=".obj";
			//listing file name will be filename.list
			string listingfilename=filename;
			listingfilename+=".list";
			//file for processing
			ifstream file;
			//file to output objectcode
			ofstream objectcode;
			//file to output listing file
			ofstream listingfile;
			//add asm extension back to file name
			filename+=".asm";
			//open file for reading.
			file.open(filename.c_str());
	
			std::cout<<"if successful, generated files will be saved as"<<endl
				 <<setw(25)<<left<<"listing:"<<listingfilename<<endl
				 <<setw(25)<<left<<"intermediate:"<<intmfilename<<endl
				 <<setw(25)<<left<<"objectfile:"<<objectfilename<<endl;

			//check if the file is open, otherwise stop, and tell the user the file was not found
			if(!file.is_open())
			{
				//source file specified was not found.
				std::cout<<"file:"<<filename<<" was not found!!"<<endl;
				return "";
			}	
			//variable declaration for symtab alphabet, and mnuemonics alphabet. 
			alphabet a;a.letters="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			//the variable representing the symboltable
			tree<int> symtab(a);
			//dont look at nearby
			symtab.searchneighbors=false;
			//the table that has the operations each node will be an array 
			//of integers, the first is just to tell how many args it takes, 
			//and the second is the op code
			a.letters="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			//a tree that holds a pair of elements for each mnuemonic.  the first is the number
			//of arguments it take, and the second is the address the mnuemonic has
			tree<vector<int> > mnuemonics(a);
			//set so the tree does not look for neighbors, which it does by default
			mnuemonics.searchneighbors=false;
			//a tree that holds the directives.  used to check if a given string is a directive
			tree<int> directives(a);
			//set so the tree does not look for neighbors.  that is a prefix of a 
			//directive would yield a result
			directives.searchneighbors=false;
			//a variable for the intermediate file, to get the actual file 
			//written all that should need to be done is to make a new file, 
			//and output intmfile
			ofstream intmfile;
			//open the file to output to
			intmfile.open(intmfilename.c_str());	
			//a function that fills the mnuemonics tree
			buildmnuemonics(mnuemonics);	
			//a function that fills the directives tree
			builddirectives(directives);
			//build the symbol table, and intermidiate file
			pass1(file, mnuemonics, directives,  symtab,  intmfile);
			//close the intermediate file
			intmfile.close();
			//close the source file
			file.close();
			//open file for pass2 to work off of
			file.open(intmfilename.c_str());
			//open file to write object code to
			objectcode.open(objectfilename.c_str());
			//open file to write listing to
			listingfile.open(listingfilename.c_str());
			//run pass2
			pass2(objectcode,listingfile,file,symtab,mnuemonics);
			//close objectcode file
			objectcode.close();
			//close intermediate file
			file.close();
			//output the symbol table in sorted order.
		//	symtab.list();
		}
	}
	return "";
}

//this updates the location counter.
void setlc(int &lc, string &instruction, string &operand, tree<vector<int> > &mnuemonics, tree<int> &directives)
{
	//instruction is a mnuemonic, so just increase lc by 3;
	if(mnuemonics.search(instruction)!=NULL)
	{
		lc+=3;
	}
	//instruction is a directive, so increase it on a case by  case basis
	else if(directives.search(instruction)!=NULL)
	{	
		int temp=0;
		//if the directive value is 0, then it takes a hex argument
		if(*(directives.search(instruction))==0)
		{
			sscanf(operand.c_str(),"%x",&temp);
		}
		//a 4 means it takes a decimal argument
		else if(*(directives.search(instruction))==4)
		{
			temp=atoi(operand.c_str());
		}	
		//set lc to the argument of start
		if(instruction=="START")
		{
			lc=temp;
		}
		//doesnt affect lc
		else if(instruction=="END")
		{
			return;
		}
		//increase lc by length of c-str, or by half of the number hex digits in the operand
		else if(instruction=="BYTE")
		{
			if(operand!=""&&toupper(operand[0])=='C')
			{
				lc+=operand.size()-3;
			}
			else if(operand!=""&&toupper(operand[0])=='X') 
			{
				lc+=(operand.size()-3)/2; 
			}
		}
		//increase lc by the length of a word, that is 3 bytes
		else if(instruction=="WORD")
		{
			lc+=3;	
		}
		//increase lc by temp many bytes
		else if(instruction=="RESB")
		{
			lc+=temp;
		}
		//increase lc by temp many words, or 3*temp many bytes
		else if(instruction=="RESW")
		{
			lc+=temp*3;
		}
	}
	else
	{
		return;
	}
}

//add the errors associated with a particular mnuemonic.  not much can be done in pass
//one because the symtab isnt filled in. this only checks if a mnuemonic needs an operand,
//then it is there.
void getmnuemonicerrors(string &label, string &instruction,string &operand, vector<char> &del, tree<vector<int> > &mnuemonics, vector<int> &errors,tree<int> &symtab)
{
	//missing operand
	if((*(mnuemonics.search(instruction)))[0]!=0&&operand=="")
	{
		errors.push_back(14);
	}
	//illegal operand
	else if(((*(mnuemonics.search(instruction))))[0]!=0&&!isvalidoperand(operand,symtab))
	{	
		errors.push_back(26);
	}
}

//add the errors associated with a given directive
void getdirectiveerrors(string &label, string &instruction, string &operand, vector<char> &del, tree<int> &directives, vector<int> &errors, tree<bool> &instructionsused,  tree<int> &symtab)
{
	int temp=0;string search;
	//missing directive operand
	if(operand=="")
	{
		errors.push_back(22);
	}
	//the directive with 0 take a hex value argument
	else if(*(directives.search(instruction))==0)
	{
		if(ishex(operand))
		{
			sscanf(operand.c_str(),"%x",&temp);
		}
		//illegal directive operand
		else 
		{
			errors.push_back(4);
		}
	}
	//a directive with 4 takes a decimal argument
	else if(*(directives.search(instruction))==4)
	{
		if(isdecimal(operand))
		{
			temp=atoi(operand.c_str());
		}

	}
	//add START directive errors
	if(instruction=="START")
	{
		search="START";
		//duplicate START statement
		if(instructionsused.search(search))
		{	
			errors.push_back(5);
		}
		//misplaced START statement
		if(instructionsused.count!=0)
		{
			errors.push_back(6);
		}
		//illegal start operand
		if(!ishex(operand))
		{
			errors.push_back(7);
		}
		//operand is too large 
		else if(temp>0x7FFF)
		{
			errors.push_back(8);
		}
		//operand is negative, and thus invalid
		else if(temp<0x0)
		{
			errors.push_back(9);
		}
		//missing name of program
		if(label=="")
		{
			errors.push_back(21);
		}
	}	
	//add the END directive errors	
	else if(instruction=="END")
	{
		search="END";
		//duplicate "end" statement
		if(instructionsused.search(search))
		{
			errors.push_back(10);
		}
		//illegal or missing operand for end
		if(!isvalidlabel(operand,symtab))
		{
			errors.push_back(11);	
		}
	}
	//add the BYTE directive errors
	else if(instruction=="BYTE")
	{
		//illegal Byte operand
		if(!((operand!=""&&toupper(operand[0])=='C')||(operand!=""&&toupper(operand[0])=='X'))) 
		{
			errors.push_back(12);
		}
		else if(operand!=""&&toupper(operand[0])=='C')
		{
			//character string too long
			if(operand.size()-3>30)
			{
				errors.push_back(16);
			}
		}
		else if(operand!=""&&toupper(operand[0])=='X') 
		{//invalid operand
			if((operand.size()-3)%2==0)
			{
				//hex string too long
				if(((operand.size()-3))>32)
				{
					errors.push_back(25);
				}
			}
			else 
			{
				//odd size hex string
				errors.push_back(15);
			}	
		}
	}
	//add the WORD directive errors
	else if(instruction=="WORD")
	{
		//invalid operand
		if(!isdecimal(operand))
		{
			errors.push_back(13);	
		}
	}
	//add the RESB directive errors
	else if(instruction=="RESB")
	{
		if(isdecimal(operand))
		{
			//operand too big
			if(temp>0x7FFF)
			{
				errors.push_back(8);
			}
		}
		//invalid operand
		else 
		{
			errors.push_back(13);
		}
	}
	//add the RESW directive errors
	else if(instruction=="RESW")
	{
		if(isdecimal(operand))
		{	
			//operand too big
			if(3*temp>0x7FFF)
			{
				errors.push_back(8);
			}
		}
		//invalid operand
		else 
		{
			errors.push_back(13);
		}
	}
}

//add all the errors that have to do with the label portion of a line.
void getlabelerrors(string &label, vector<char> &del, tree<int > &symtab, tree<vector<int> > &mnuemonics, tree<int> &directives, vector<int> &errors)
{
	//illegal label
	if(!isvalidlabel(label,symtab))
	{
		errors.push_back(0);
	}
	//too many labels	
	if(label!=""&&symtab.count+1>500)
	{
		errors.push_back(1);
	}
	//duplicate label;
	if(symtab.search(label)!=NULL||mnuemonics.search(label)!=NULL||directives.search(label)!=NULL)
	{
		errors.push_back(2);
	}
}

//add all the errors that have to do with the operand itself.  put them at the end of
//the errors string
void getoperanderrors(string &errors,string &instruction, string &operand, tree<int> &symtab, tree<vector<int > > &mnuemonics)
{
	vector<char> del;del.push_back(',');
	string op;
	op=functiontree::getfirsttoken(operand,del);
	//missing operand
	if(mnuemonics.search(instruction)!=NULL&&(*(mnuemonics.search(instruction)))[0]>0&&operand=="")
	{
		errors+="23 ";
	}
	//invalid operand
	else if(mnuemonics.search(instruction)!=NULL&&(*(mnuemonics.search(instruction)))[0]>0&&!(isvalidlabel(op,symtab)||isvalidlabel(operand,symtab))&&!(ishex(op)||ishex(operand)))
	{
		errors+="27 ";
	}
	//undefined operand
	else if(mnuemonics.search(instruction)!=NULL&&(*(mnuemonics.search(instruction)))[0]>0)
	{
		if(!isindexed(operand)&&!ishex(operand)&&symtab.search(operand)==NULL)
		{
			errors+="24 ";
		}
		else if(isindexed(operand)&&!ishex(op)&&symtab.search(op)==NULL)
		{
			errors+="24 ";
		}
	}
}

//add all the erorrs that dont depend on any particular line, such as program size, and things
//that are missing.  
void getfinalpass1errors(vector<string> &instructions, vector<int> &errors, tree<bool> &instructionsused, int &start, int& end)
{
	string search="";
	//program too long, or underflow
	if(end-start>32768||end-start<0)
	{
		errors.push_back(17);
	}
	//missing end statement
	search="END";
	if(instructionsused.search(search)==NULL)
	{
		errors.push_back(18);
	}
	//misplaced end statement
	if(toupper(instructions.back())!="END"&&instructionsused.search(search)!=NULL)
	{
		errors.push_back(19);
	}
	search="START";
	//misplaced start statement
	if(toupper(instructions.front())!="START"&&instructionsused.search(search)!=NULL)
	{
		errors.push_back(20);
	}
}

//doesnt do anything because i couldnt think of anymore errors.  its mainly here
//for symmetry in the passes
void getfinalpass2errors(string &errors)
{

}

//insert the directive into the directive tree.  just grunt work.
void builddirectives(tree<int>& directives)
{
	//the name of the instruction
	string name="";
	//denotes argument type
	int data;

	name="START";
	data=0;
	directives.insert(data,name);

	name="END";
	data=1;
	directives.insert(data,name);

	name="BYTE";
	data=2;
	directives.insert(data,name);

	name="WORD";
	data=2;
	directives.insert(data,name);

	name="RESB";
	data=4;
	directives.insert(data,name);

	name="RESW";
	data=4;
	directives.insert(data,name);
}

//puts the mnuemonics, and directives in the mnuemonics table
//pretty much just defines the mnuemonics.  the data is
//a vector of two elements.  the first is the number of arguments
//it takes, and the second is the op code for the mnuemonic
void buildmnuemonics(tree<vector<int> >& mnuemonics)
{
	string name;
	vector<int> data;

	data.clear();
	data.push_back(1);data.push_back(0x18);	
	name="ADD";
	mnuemonics.insert(data, name);

	data.clear();
	data.push_back(1);data.push_back(0x58);	
	name="AND";
	mnuemonics.insert(data, name);
	
	data.clear();
	data.push_back(1);data.push_back(0x28);	
	name="COMP";
	mnuemonics.insert(data, name);
	
	data.clear();
	data.push_back(1);data.push_back(0x24);	
	name="DIV";
	mnuemonics.insert(data, name);
	
	data.clear();
	data.push_back(1);data.push_back(0x3C);	
	name="J";
	mnuemonics.insert(data, name);
	
	data.clear();
	data.push_back(1);data.push_back(0x30);	
	name="JEQ";
	mnuemonics.insert(data, name);
	
	data.clear();
	data.push_back(1);data.push_back(0x34);	
	name="JGT";
	mnuemonics.insert(data, name);
	
	data.clear();
	data.push_back(1);data.push_back(0x38);	
	name="JLT";
	mnuemonics.insert(data, name);
	
	data.clear();
	data.push_back(1);data.push_back(0x48);	
	name="JSUB";
	mnuemonics.insert(data, name);
	
	data.clear();
	data.push_back(1);data.push_back(0x00);	
	name="LDA";
	mnuemonics.insert(data, name);
	
	data.clear();
	data.push_back(1);data.push_back(0x50);	
	name="LDCH";
	mnuemonics.insert(data, name);
	
	data.clear();
	data.push_back(1);data.push_back(0x08);	
	name="LDL";
	mnuemonics.insert(data, name);
	
	data.clear();
	data.push_back(1);data.push_back(0x04);	
	name="LDX";
	mnuemonics.insert(data, name);
	
	data.clear();
	data.push_back(1);data.push_back(0x20);	
	name="MUL";
	mnuemonics.insert(data, name);
	
	data.clear();
	data.push_back(1);data.push_back(0x44);	
	name="OR";
	mnuemonics.insert(data, name);
	
	data.clear();
	data.push_back(1);data.push_back(0xD8);	
	name="RD";
	mnuemonics.insert(data, name);
	
	data.clear();
	data.push_back(0);data.push_back(0x4C);	
	name="RSUB";
	mnuemonics.insert(data, name);
	
	data.clear();
	data.push_back(1);data.push_back(0x0C);	
	name="STA";
	mnuemonics.insert(data, name);
	
	data.clear();
	data.push_back(1);data.push_back(0x54);	
	name="STCH";
	mnuemonics.insert(data, name);
	
	data.clear();
	data.push_back(1);data.push_back(0x14);	
	name="STL";
	mnuemonics.insert(data, name);
	
	data.clear();
	data.push_back(1);data.push_back(0x10);	
	name="STX";
	mnuemonics.insert(data, name);
	
	data.clear();
	data.push_back(1);data.push_back(0x1C);	
	name="SUB";
	mnuemonics.insert(data, name);
	
	data.clear();
	data.push_back(1);data.push_back(0xE0);	
	name="TD";
	mnuemonics.insert(data, name);
	
	data.clear();
	data.push_back(1);data.push_back(0x2C);	
	name="TIX";
	mnuemonics.insert(data, name);
	
	data.clear();
	data.push_back(1);data.push_back(0xDC);	
	name="WD";
	mnuemonics.insert(data, name);

}

//put a T, and the start adress in the cooresponding collumns of the text 
//record
void initializetextrecord(string &record,string &lcs)
{
	record="T00000000";
	for(int i=0;i<lcs.size();i++)
	{
		record[6-i]=lcs[lcs.size()-1-i];
	}
}

//the records are only gonna be output when all of them are built.
//there really wont ever be too many records.  if the entire program were
//executable lines, it would at most be 500 text records, which really isnt
//that much to store in memory.  the header and end records will be the last 
//two records stored in the records vector.  first is header, then end 
//records must have at least the header record, and end record in it before 
//we come to this
void writerecords(vector<string> &records, ofstream &output)
{
	//ouput the header record, which is the second to last
	//record in  records
	output<<records[records.size()-2]<<endl;
	//output all the text records in order
	for(int i=0;i<records.size()-2;i++)
	{
		output<<records[i]<<endl;
	}
	//ouput end record
	output<<records[records.size()-1];
}

//find the size of the record, and write it to record
void finalizetextrecord(string &record, int &start, int &end)
{
	int size= end-start;
	string temp;
	temp= tohex(size);
	for(int i=0;i<temp.size();i++)
	{
		record[8-i]=temp[temp.size()-1-i];
	}
}

//the intermediate file will have seven lines for each line of code:
// the first is an echo of the line, 
//the second is the line number
//the third is the location counter, 
//the fourth is the instruction, 
//the fifth is the operand, or a blank line if none is present
//the sixth line is a list of numbers cooresponding to errors on the line, each separated by a space
//the seventh is any comment.  
//if the line is just a comment, the instruction, and operand field will be blank.  
//at the end of the intermediate file, another 7 lines will be added,first is program name, then
//second is the start address of the program, third is
//program size, and fourth is ****, fifth is end label, sixth is the errors 
//that werent caused by a specific line, last comment.
void writeintmfileln(string &currentline, int &linenumber, int &lc, string &instruction, string &operand, string &comment, vector<int> &errors, ofstream &intmfile)
{
	intmfile<<currentline<<endl;
	intmfile<<dec<<linenumber<<endl;
	//location counter is output in hex
	intmfile<<hex<<lc<<endl;
	intmfile<<instruction<<endl;
	//operand is either a label, or hex value, or hex,X
	intmfile<<hex<<operand<<endl;
	//output all the errors for the line, seperated by spaces.
	for(int i=0;i<errors.size();i++)
	{
		intmfile<<dec<<errors[i]<<" ";
	}
	intmfile<<endl;
	intmfile<<comment<<endl;
}

//write out to the listing file: the line, then the objectcode,then load address, 
//and then errors for that line
void writelistingfileln(ofstream &listingfile,string &currentline,string &objectcode,string &loadaddress,string &errors)
{
	vector<char> del; del.push_back(' ');
	string errormessage="";
	string swap;
	listingfile<<"THE Source Line was: "<<currentline<<endl;
	listingfile<<"THE Object Code Generated was: "<<objectcode<<endl;
	listingfile<<"The Load Address was: "<<loadaddress<<endl;
	listingfile<<"THe Errors are: "<<endl;
	if(errors!="")
	{
		errormessage=geterrormessage(atoi(functiontree::getfirsttoken(errors,del).c_str()));
		swap=functiontree::removefirsttoken(errors,del);
		listingfile<<errormessage<<endl;
		while(swap!="")
		{
			swap=functiontree::removefirsttoken(swap,del);
			listingfile<<errormessage<<endl;
			errormessage=geterrormessage(atoi(functiontree::getfirsttoken(swap,del).c_str()));
		}
	}
	listingfile<<endl;
}

//output symbol table to the listing file, just makes a call to recursive function
void writesymtabtolistingfile(ofstream &listingfile, tree<int> &symtab)
{
	listingfile<<"SYMBOL TABLE IS:"<<endl;
	writesymtabtolistingfile(listingfile, symtab.root,"",symtab);
}

//recursive function that outputs the label names, and address.
//pretty much the same as the list function in the tree class
//with listingfile instead of cout
void writesymtabtolistingfile(ofstream &listingfile, Node<int> *current, string key,tree<int> &symtab)
{
	if(current==NULL)
	{
		return;
	}
	else
	{
		if(current->data!=NULL)
		{
			listingfile<<*(current->data)<<" "<<key<<endl;
		}
		for(int i=0;i<symtab.table.letters.size();i++)
		{
			writesymtabtolistingfile(listingfile,current->children[i],key+symtab.table.letters[i],symtab);
		}
	}
}

//the actual pass one function of the assembler.  this is called by assembler 
//to build the symbol table, and the intermediate file.  there is a bit of 
//redundant work within functions but keeping it as is seemed like it would be
//easier to understand, and read.  i wanted this function to be mostly calls to 
//other functions and data declaration to make it easier to understand.  
void pass1(ifstream &in, tree<vector<int> > &mnuemonics, tree<int> &directives, tree<int> &symtab, ofstream &intmfile)
{
	//this is here because the tree template wants everything by reference.	
	bool exist=true;
	//this just a set of the instructions that have been used so far
	tree<bool> instructionsused(mnuemonics.table);
	instructionsused.searchneighbors=false;
	
	//the number of current line, starting at 0
	int linenumber=0;
	//location counter
	int lc=0;
	//start address of program
	int start=0;
	//end address of program
	int end=0;
	
	//the current line of the source
	string currentline="";
	//the label contained in the current line of the source
	string label="";
	//the mnuemonic, or directive of the current line
	string instruction="";
	//the operand of the current line
	string operand="";
	//any comments on the current line
	string comment="";
	//used to get operand, and instruction without ruining the currentline
	//because getnexttoken takes a string by refference
	string temp="";
	//the name of the source program
	string name="";
	//the label used with the end statement.
	string endlabel="";
	//last instruction used
	string lastinstruction="";

	//the errors on the current line
	vector<int> errors;
	//the first and last instruction used by the source.
	vector<string> instructions;	
	//deliminators used by getnexttoken
	vector<char> del;del.push_back(' ');del.push_back('\t');

	//bool variable set to true, then there are no more lines to process 
	bool done=false;

	//run til the file is done, or an end statement is reached.
	while(in&&!done)
	{
		//clear the fields, so if a particular one is not found it is ""
		label=instruction=operand=comment=temp="";
		//clear any errors added by the previous line
		errors.clear();
		//get a line of the source file, and put it in currentline
		getline(in,currentline);
		//increment the linenumber
		linenumber++;
		//if it is a blank line, ignore, otherwise, proccess line.
		if(currentline!=""){
			//if the line is a comment, then set the comment field to the currentline in the intermediate file.
			if(iscomment(currentline))
			{
				comment=currentline;
				//write the comment to the intm file
				writeintmfileln(currentline, linenumber,lc,instruction,operand,comment,errors,intmfile);	
			}
			//otherwise set lc, instruction, operand, comment, errors, and label.
			else{
				//set temp to current line, so it can be torn apart
				temp=currentline;
				//check if the line has a label
				if(haslabel(currentline,del))
				{
					//set the label field.  if there is a label, then it is the first token in the line
					label=functiontree::getfirsttoken(currentline,del);	
					//labels are not case sensitive, so make everything uppercase
					label=toupper(label);
					//if there is a label, then get all the errors that are cause by that label.
					getlabelerrors(label, del, symtab,mnuemonics,directives,errors);
					//remove the label from the current line, and let 	
					temp=functiontree::removefirsttoken(currentline,del);
				}
				//if there was a label, then it was removed from temp already, so instruction is the first token
				instruction=functiontree::getfirsttoken(temp,del);
				//instructions are not case sensitive
				instruction=toupper(instruction);
				//remove the instruction from temp.
				temp=functiontree::removefirsttoken(temp,del);
				//check if the instruction has an operand, and if it does, get it, and remove it from temp
				if(hasoperand(instruction,mnuemonics,directives))
				{
					//get the operand from temp
					operand=functiontree::getfirsttoken(temp,del);
					//remove the operand from temp
					temp=functiontree::removefirsttoken(temp,del);
				}
				//the rest of temp is just comment
				comment=temp;
				//if the instruction is a muemonic, then set the mnuemonic errors that we can
				if(mnuemonics.search(instruction)!=NULL)
				{
					//set the mnuemonicerrors
					getmnuemonicerrors(label,instruction,operand,del,mnuemonics,errors,symtab); 				
				}
				//if the instruction is a directive set any errors that are associated with the directive
				else if(directives.search(instruction)!=NULL)
				{
					//add all the directive errors
					getdirectiveerrors(label,instruction,operand,del,directives,errors,instructionsused,symtab);
				}
				//if an instruction is not a directive, or mnuemonic, then it is an invalid instruction
				else
				{	
					//add the error invalid instrucion to the list
					errors.push_back(3);
				}
				//after we know what instruction we are working with, we can decide whether
				//to insert the label into the symtab, or save it as the program name
				if(haslabel(currentline,del))
				{		
					//if the instruction is start, then the label field is the name of the program
					if(instruction=="START")
					{
						name=label;
						//set the start address 
						sscanf(operand.c_str(),"%X",&start);
					}
					else
					{
						//if there are no problems with the label, then we can put it into the symbol table
						if(errors.empty())
						{
							//insert the label into the symbol table.  the label is used as a key to
							//the radix tree, and the location counter is the value stored there.  
							//this means that if we search the tree for a key and get null, the 
							//label has not been used.  the labels can be retrieved in sorted order
							symtab.insert(lc,label);
						}
					}
				}
			}
			//if the instruction valid, then put it into a list 
			if(mnuemonics.search(instruction)!=NULL||directives.search(instruction)!=NULL)
			{	
				//if this is the first valid instruction, then add it to instructions
				if(instructions.size()==0)
				{
					instructions.push_back(instruction);
				}
				//if the instruction is valid, then it is the most recent instruction, so set
				lastinstruction=instruction;
				//insert the instruction into the instructions used tree.  this is to check for duplicate
				//start, end.  this could probably have been done much simplier, but would have had more
				//error checking in this function, or more parameters passed the the error checking functions
				instructionsused.insert(exist,instruction);		
			}
			
			//write to the intermediate file in the format specified in the comments of this function
			writeintmfileln(currentline,linenumber,lc,instruction,operand,comment,errors,intmfile);
			//check if we are at the end of the source
			if(instruction=="END")
			{
				done=true;
				end=lc;
				endlabel=operand;
			}
			//update the location counter
			setlc(lc,instruction,operand,mnuemonics,directives);	
		}
	}	
	//once we are done processing the file, last instruction will be the last valid instruction used. add it to instructions
	instructions.push_back(lastinstruction);
	//clear the errors, so we can add so we can add the finalpass1 errors to it
	errors.clear();
	//get the errors that dont pertain to a specific line 
	getfinalpass1errors(instructions,errors,instructionsused,start, end);
	//set current line to the program name
	currentline=name;
	//set the lc to the program size
	lc=end-start;
	//this is just so in pass two it is really easy to tell that this is gonna be used for the header record.
	instruction="****";
	//set the operand to the first executable statement of the program
	operand=endlabel;
	//leave a note mentioning this isnt a source line we processed
	comment="this is just for program size, etc. not generate from any particular source line";
	//output these to the intm file.
	writeintmfileln(currentline,start,lc,instruction,operand,comment,errors,intmfile);
}

//second pass of the assemble function.  this creates the listingfile, and 
//object file names output.txt.  it does not output to output.txt if there are 
//errors, but the last succesful build will still be there
void pass2(ofstream &output, ofstream &listingfile, ifstream &intmfile, tree<int> &symtab, tree<vector<int> > &mnuemonics)  
{

	mnuemonics.searchneighbors=false;

	string line="";
	string record="";
	string errors="";
	string instruction="";
	string operand="";
	string lcs="";
	string lns="";
	string comment="";
	string objectcode="";

	int lc=0;	
	int start=0;
	int linenumber=0;
	int objectcodelength=0;

	vector<string> records;

	bool fail=false;	

	while(intmfile)
	{
		objectcode="";

		//first line echo of the line, is ignored
		getline(intmfile, line);
		//second line is line number in decimal
		getline(intmfile, lns);
		sscanf(lns.c_str(),"%d",&linenumber);
		//third line location counter in hex
		getline(intmfile, lcs);
		sscanf(lcs.c_str(),"%x",&lc);
		//fourth line instruction
		getline(intmfile, instruction);
		//fifth line operand
		getline(intmfile, operand);
		//sixth line errors
		getline(intmfile, errors);
		//seventh line comment
		getline(intmfile, comment);
		//this is when we are on the last part of intm file.  
		//this is not actually part of source, so it does not need to be processed
		if(instruction=="****")
		{
			break;
		}
		//if the instruction is a mnuemonic
		if(mnuemonics.search(instruction)!=NULL)
		{
			if(!istextrecordinitialized(record))
			{
				initializetextrecord(record,lcs);
				start=lc;
			}
			if(!hasroomformnuemonic(record))
			{
				//lc- start address of the record is the size
				finalizetextrecord(record,start,lc);
				records.push_back(record);
				start=lc;
				//lc is the start adress of the text record
				initializetextrecord(record,lcs);
			}
			//get the operand errors and put them at the back of the errors string
			getoperanderrors(errors,instruction,operand,symtab,mnuemonics);
			//if there are no errors, then generate object code
			if(errors.size()==0)
			{
				objectcode=getmnuemonicobjectcode(instruction,operand,symtab,mnuemonics,lc);
				record+=objectcode;
			}
			//otherwise set fail to true, so that nothing is output to output.txt
			else 
			{
				fail=true;
			}	
		}
		//check if the instruction is BYTE, and generate appropriate object code
		else if(instruction=="BYTE")
		{
			//This allows us to know the size of the Text record fairly easily
			if(!istextrecordinitialized(record))
			{
				initializetextrecord(record,lcs);
				start=lc;
			}
			//check if the record has room for the object code.  if not 
			//start a new text record
			if(!hasroomforbyte(record,operand))
			{
				//lc- start address of the record is the size
				finalizetextrecord(record,start,lc);
				records.push_back(record);
				start=lc;
				//lc is the start adress of the text record
				initializetextrecord(record,lcs);
			}
			objectcode=getbyteobjectcode(operand);
			record+=objectcode;
		}
		//check if the instruction is WORD, and generate appropriate object code
		else if(instruction=="WORD")
		{
			if(!istextrecordinitialized(record))
			{
				initializetextrecord(record,lcs);
				start=lc;
			}
			if(!hasroomformnuemonic(record))
			{
				//lc- start address of the record is the size
				finalizetextrecord(record,start,lc);
				records.push_back(record);
				start=lc;
				//lc is the start adress of the text record
				initializetextrecord(record,lcs);
			}
			objectcode=getwordobjectcode(operand);
			record+=objectcode;
		}	
		//check if the instructions dont generate object code
		else if(instruction=="RESW"||instruction=="RESB"||instruction=="END")
		{
			//if the record is already initialized, then save the record
			//and set the record to blank, so it will start a new record
			//when it hits an instruction that generates object code, it
			//will start a new record.
			if(istextrecordinitialized(record))
			{
				//lc- start address of the record is the size
				finalizetextrecord(record,start,lc);
				records.push_back(record);
				record="";
			}
		}
		//only ouput the load address if there is object code
		if(objectcode=="")
		{
			lcs="";
		}
		writelistingfileln(listingfile,line,objectcode,lcs,errors);	
	}
	//get and other errors, and put them on errors
	getfinalpass2errors(errors);
	//output errors that are not part of any line
	writelistingfileln(listingfile,line,objectcode,lcs,errors);
	//output the symbol table at the end of the listing file.
	writesymtabtolistingfile(listingfile,symtab);
	if(errors!="")
	{
		fail=true;	
	}
	//write out the object code, only if the program was error free
	if(!fail)
	{
		//write header record// current line is the name of the program, lc is the size
		//and linenumber is the starting address of the program in decimal.
		record=getheaderrecord(line,lcs,linenumber);
		records.push_back(record);
		//write end record.  operand is the end operand, so it should be the first executable 
		//line in the program.
		record=getendrecord(operand,symtab);
		records.push_back(record);
		//output the object code
		writerecords(records,output);
		std::cout<<"\aobject file successfully generate"<<endl;
	}
	else 
	{
		//give message that there were errors while building file.
		std::cout<<"\a\a\aThere were errors while generating the object file!!!  Please see listing file for details"<<endl;
	}
}

