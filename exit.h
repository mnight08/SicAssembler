#include <string>
//ends the simulator. because engine.cpp automatically compiles and runs extensions
//again, this has to exit with code 0.  engine checks the exit status, and if it 
//is zero, it stops.
string exit(string& arg)
{
	if(arg!="")
	{
		std::cout<<"too many arguments, the function takes no arguments"<<endl;
		return "";
	}
	std::cout<<"you exited the program, congrats!!!!!!"<<endl;
	exit(0);

	return string("");

}
