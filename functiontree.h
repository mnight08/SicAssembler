#ifndef FUNCTIONTREE
#define FUNCTIONTREE
#include "tree.h"
#include "alphabet.h"
#include <string>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "insertnodes.h"
#include <vector>
//void insertnodes(tree<string (*)(string&)>*&);
class functiontree
{
	public:
		//this should be more general, and tree should be able to be any container type
		tree<string (*)(string&)> *functions;
		//this function makes the tree, it takes in two file names, the first is a file containing function prototypes of the functions to be put in the tree, the other is a list of flags that affect what it will do.				
		void build(string &prototypes,string &flag, string &includelist);	
		static string getsubstring(int,int, string&);
		//returns the given string minus its first token
		static string removefirsttoken(string&,vector<char>);
		//return the first token of the given string
		static string getfirsttoken(string&,vector<char>);
		string getname(string&);
		functiontree();
};

functiontree::functiontree()
{
	alphabet temp;
	temp.letters="abcdefghijklmnopqrstuvwxyz";
	functions= new tree<string(*)(string&)>(temp);
}

//this is a static function that takes a string, and a list of separators, and returns the string 
//minus the first set  of continous characters that are no in del
string functiontree::removefirsttoken(string& line,vector<char> del)
{
	bool isdel=false;
	if(line=="")
		return "";
	int i=0;
	int start=0;
	for(;i<line.size();i++)
	{
		//i forgot this line in phase 1. if you dont reset isdel to false, then it 
		//wont work right.
		isdel=false;
		for(int j=0;j<del.size();j++)
		{
			if(line[i]==del[j])
			{
				isdel=true;
				break;
			}
		}
		if(!isdel)
		break;
	}
	for(;i<line.size();i++)
	{	
		isdel=false;
		for(int j=0;j<del.size();j++)
		{
			if(line[i]==del[j])
			{
				isdel=true;
				break;
			}
		}
		if(isdel)break;
	}
	for(;i<line.size();i++)
	{
		isdel=false;
		for(int j=0;j<del.size();j++)
		{
			if(line[i]==del[j])
			{
				isdel=true;
				break;
			}
		}
		if(!isdel)
		break;
	}
	start=i;
	return getsubstring(start, line.size(),line);
}

//similiar to the above function, except it returns the  first set of continous characters
//that are not in del
string functiontree::getfirsttoken(string& line,vector<char> del)
{
	bool isdel=false;
	int i=0;int start=0; int end=line.size();
	for(;i<line.size();i++)
	{	
		isdel=false;
		for(int j=0;j<del.size();j++)
		{
			if(line[i]==del[j])
			{
				isdel=true;
				break;
			}
		}
		if(!isdel)break;
	}
	start=i;isdel=false;
	for(;i<line.size();i++)
	{
		isdel=false;
		for(int j=0;j<del.size();j++)
		{
			if(line[i]==del[j])
			{
				isdel=true;
				break;
			}
		}
		if(isdel)break;
	}
	end=i;
	return getsubstring(start,end, line);
		
} 

//takes a function prototype and gets the function name. this is not very general
//but hasnt failed yet
string functiontree::getname(string &prototype)
{
	int i=0;
	string name="";
	//find space
	for(;i<prototype.size();i++)
	{	
		if(prototype[i]==' ')
		break;
	}	
	i++;
	while(prototype[i]!='(')
	{
		name=name+prototype[i];
		i++;
	}
	return name;
}

//returns a substring of arglist from begin to end
string functiontree::getsubstring(int begin, int end, string&  arglist)
{
	string sub="";
	for(int i=begin;i<end;i++)
	{
		sub=sub+arglist[i];
	}
	return sub;
}


//this is the function that does the messy work.  it is only really neccesary so that new functoins could be inserted from inside the simulator first it will check flags.txt, if its
// number is zero, then it will read from the function prototype list, and make a function that inserts nodes into the tree
//if the number is one, then it will run the function that inserts the nodes into the function.
void functiontree::build(string &prototypes,string &flag, string &includelist)
{
	//this may be a bit confusing, so it will be very heavily commented
	
	//this will be a file named prototypes, we assume that each line is of the form "string func(string&);	
	fstream functionlist;

	//this will be a file that is initially empty, build will fill this file with code that inserts each function into the tree,  then it will be compiled, and ran.
	fstream insertnodesf;

	//this is a file that tells build whether to rewrite insertnodes, or to run the function 
	fstream flags;

	//this is a list of all the function header that are custom made
	fstream include;
	flags.open(flag.c_str());
	if(!flags.is_open())
		std::cout<<"flags failed to open"<<endl;
	int temp;
	flags>>temp;
	if(temp==0)
	{
		std::cout<<"flags was 0, rebuilding the command list"<<endl;
		functionlist.open(prototypes.c_str());
	//	if(remove("insertnodes.h")!=0);
	//		std::cout<<"insertnodes.h failed to delete"<<endl;
		insertnodesf.open("insertnodes.h",ios::in);
			
		if(!insertnodesf.is_open()){
			std::cout<<"insertnodes.h failed to open"<<endl;
			exit(0);
		}
		string temp;
		int i=0;		
		while(insertnodesf)
		{
			getline(insertnodesf, temp);	
			i++;
		}
		insertnodesf.close();
		insertnodesf.open("insertnodes.h",ios::out);
		for(int j=0;j<i;j++)
		{
			insertnodesf<<"                                                                                                                                                           ";
			insertnodesf<<"\n";
		}
		insertnodesf.seekp(0);
		include.open(includelist.c_str());
		if(!include.is_open())
			std::cout<<"include list failed to open"<<endl;
		include.seekp(0);
		insertnodesf<<"#include \"prototypes.h\"\n#include \"alphabet.h\"\n#include \"tree.h\"\nvoid insertnodes(tree<string (*)(string&)>*& container){\n";
		//ideally there would be a function to scan the prototypes file, and get every letter that is used, then make the alphabet based on that, this is just a bit easier to do, might make it more general latter
		insertnodesf<<"alphabet table;table.letters=\"abcdefghijklmnopqrstuvwxyz\";\nstring (*funct)(string&)=NULL;\nstring temp=\"\";\n"<<"//this function was generated by build in functiontree\n";
		//loop should start here
		string prototype;
		while(functionlist)
		{
			
			getline(functionlist, prototype);
			//kinda a weak check to see if the line is actually a function prototype, and not a comment or something.
			if(prototype[0]=='s')
			{
				string key=getname(prototype);
				std::cout<<"function "<<prototype<<" was inserted into the tree"<<endl;
				include<<"#include \""<<key<<".h\"\n";
							
				insertnodesf<<"temp=\""<<key<<"\";\nfunct="<<key<<";\n";
				insertnodesf<<"container->insert(funct,temp);\n";
			}	
		}
		insertnodesf<<"}";
		flags.seekp(0);
		flags<<1;
		flags.close();
		functionlist.close();
		insertnodesf.close();
		include.close();
		flags.close();
		exit(1);
	}
	else 
	{
		insertnodes(functions);

	}

}
#endif
