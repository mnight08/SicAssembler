#include <string>
// a function stub for the debug command
string debug(string &arg)
{
	std::cout<<"the function \"debug\" is just a stub"<<endl;
	if(arg!="")
		std::cout<<"too many arguments, the function takes no arguments"<<endl;
	return "";
}
