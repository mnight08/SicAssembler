#include <string>
#include "functiontree.h"
//a function stub for the dump command
string dump(string &arg)
{
	vector<char> del;del.push_back(' ');del.push_back('\t');
	functiontree temp;
	string temp2;
	std::cout<<"the function \"dump\" is just a stub"<<endl;
	if(arg==""||temp.removefirsttoken(arg,del)=="")
		std::cout<<"too few arguments, the function takes two arguments"<<endl;
	temp2=temp.removefirsttoken(arg,del);
	if(temp.removefirsttoken(temp2,del)!="")
		std::cout<<"too many arguments, the function only takes two arguments"<<endl;	
	return "";
}
