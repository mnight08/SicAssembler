#include <string>
using namespace std;
//treat all args and return types as strings.  then each function will be of the form string name(int argcount, string arguments);
//this is used by the build function to rewrite the insertnodes function.  if the user wants to add a new function all that they need
//to do is set flags.txt to 0, insert the function prototype string name(string&); at the end of this list, and make the file
//name.h with the function inside of it.
string exit(string&);
string directory(string&);
string help(string&);
string load(string&);
string execute(string&);
string debug(string&);
string dump(string&);
string assemble(string&);
string test(string&);
