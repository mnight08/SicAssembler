
#ifndef TREE
#define TREE
#include "Node.cpp"
#include "alphabet.h"
template<class type>
//this class implements what i think is a radix tree.  i tried to keep it general
//but it is probably not as general as it could be
//insertion, and search are based soley on string keys, associated with a given data value
class tree
{
	public:
		bool searchneighbors;
		//a table where each letters integer value can be looked up
		alphabet table;
		//the root of the tree
		Node<type> * root;
		//returns true if an item was successfully inserted into the tree
		bool insert(type, string&);
		//called by insert to insert an item into the tree
		bool recinsert(type, string&, int, Node<type>*&);
		//returns a pointer to the object corresponding to the key, and NUll if it is not found
		type* search(string&);
		//search for functions with parents of the given node
		type* nearby(Node<type>*);
		//constructor for tree
		tree(alphabet&);
		//list all the nodes in the tree, along with the key
		void list();
		//recursive list to make listing simplier to code
		void list(Node<type>*,string);
		//number of total items in the tree
		int count;
};

template <class type>
//uses the key as a path through a set of children nodes, til it is at the end of the key,
//then it inserts the item to the last node it is on
bool tree<type>:: recinsert(type tobeinserted,string & key, int letter, Node<type>*& current)
{
		if(current ==NULL)
		{
			current = new Node<type>(table.letters.size());
			if(key.size()==letter)
			{
				if(current->data==NULL)
					current->data= new type;
					
					*(current->data)=tobeinserted;
					current->count++;
					count++;

					return true;
			}
			else{
				if(table.getnum(key[letter])==-1)
					return false;
				else
					return recinsert(tobeinserted,key,letter+1,current->children[table.getnum(key[letter])]);
			}
		}
		else
		{
			if(key.size()==letter){
				current->count++;return true;}
			else if(letter>key.size()||table.getnum(key[letter])==-1)
				return false;
			else
			 return recinsert(tobeinserted,key,letter+1,current->children[table.getnum(key[letter])]);
		}
}

template <class type>
void tree<type>::list()
{
	list(root,"");

}

template <class type>
void tree<type>::list(Node <type> *current, string key)
{
	if(current==NULL)
		return;
	else
	{
		if(current->data!=NULL)	{cout<<*(current->data)<<" "<<key<<endl;}
		for(int i=0;i<table.letters.size();i++)
		{
			list(current->children[i],key+table.letters[i]);
		}
	}
}

template <class type>
tree<type>::tree(alphabet& input)
{
	root=NULL;count=0;
	table.letters=input.letters;
	searchneighbors=true;
}


template <class type>
bool tree<type>::insert(type tobeinserted, string& key)
{
	if(key=="")
		return false;
	else
	return recinsert(tobeinserted,key,0,root);
}

template <class type>
type* tree<type>::search(string& key)
{
	Node<type>* current=root;
	for( int i=0;i<key.size();i++)
	{
		if(current==NULL)
		{
			return NULL;
		
		}
		if(table.getnum(key[i])==-1)
			return NULL;		
		current=current->children[table.getnum(key[i])];
	}

	if(current==NULL||current->data==NULL)
	{
		if(!searchneighbors)
			return NULL;
	
		else if(current!=NULL)
			return nearby(current);
		else return NULL; 
	}
	else return current->data;
	
}

//search for a string that has the one that led to current as a substring
template <class type>
type* tree<type>::nearby(Node<type>* current)
{
	if(current==NULL)
	{
		return NULL;
	}
	else if(current->data!=NULL)
	{
		return current->data;
	}
	else
	for( int i=0;i<table.letters.size();i++)
	{
		type* temp=nearby(current->children[i]);
		if(temp!=NULL)
			return temp;
	}
}
#endif
