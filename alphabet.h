#ifndef alph
#define alph
//this class provides a way of indexing a given letter to a fixed set of numbers. 
//all it really does is linear search for the letter.  this is used for tree.h
class alphabet
{
	public:
		string letters;
		int getnum(char letter);	
		alphabet();
};
alphabet::alphabet()
{
	letters="";
}

//return the index the letter is at, or -1 if it is not found.
int alphabet::getnum(char letter)
{
	for( int i=0;i<letters.size();i++)
		if(letters[i]==letter)
		return i;
	return -1;
}
#endif
