/*****************************************************/
//Alex martinez 10308748
//Phase 3
/*****************************************************/
#include <stdlib.h>
#include <iostream>
using namespace std;
//this is the main program of the simulator, this is the file that should be compiled and ran
//this program in turn compiles extensions, and runs it.  it will recompile it every time the 
//function exits unless it exits with a 0 code.  a nice thing about this is the simulator wont crash if
//an individual function in it causes a segmentation fault, because it would automatically be recompiled
//and run again.
int main()
{
	system("g++ -o extensions extensions.cpp");
	//had to google how to get the right exit code, but this seems to work.
	while(system("./extensions")>>8!=0)
	{
		system("g++ -o extensions extensions.cpp");
	
	}
	

}
