#include <string>
#include "functiontree.h"
//a stub for running the load command.
string load(string& arg)
{
	functiontree temp;
	vector<char> del;del.push_back(' ');del.push_back('\t');
	std::cout<<"the function \"load\" is just a stub"<<endl;
	if(arg=="")
		std::cout<<"too few arguments, the function takes one argument"<<endl;
	if(temp.removefirsttoken(arg,del)!="")
		std::cout<<"too many arguments, the function only takes one argument"<<endl;	
	return "";
}
